#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>

#include <iostream>
#include <string>

#include "router.h"
#include "handler.h"

#include "Iconnection.h"

namespace tcp
{
	class connection
		: public Iconnection
	{
	public:
		using pointer = boost::shared_ptr<connection>;
		static pointer create(boost::asio::io_service& io_service);

		//Iconnection
		boost::asio::ip::tcp::socket& socket() override;
		void handle_input_message() override;
		void handle() override;
		std::string& output_msg() override;

	private:
		connection(boost::asio::io_service& io_service);
		static constexpr size_t max_length{ 1<<10 };
		boost::asio::ip::tcp::socket socket_;
		std::string output_message;
		std::string input_message;
		boost::asio::streambuf input_buffer;

		Request::Handler handler_;
		Request::Router router_;

	};
} //namespace tcp
