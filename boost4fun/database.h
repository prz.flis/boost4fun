#pragma once
#include <atomic>
#include <iostream>

class database_impl
{
	std::atomic_int32_t counter{};
public:
	void increment()
	{
		++counter;
		if (counter % 1000 == 0)
			std::cout << counter << std::endl;
	}
};

class database
{

	static database_impl instance;
public:
	static database_impl& get()
	{
		return instance;
	}
};