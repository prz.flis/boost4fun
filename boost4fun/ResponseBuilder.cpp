#include "ResponseBuilder.h"
#include <sstream>

Response::ResponseBuilder::ResponseBuilder(const Response::Status status)
	: status{ status }
{

}

std::string Response::ResponseBuilder::getJsonString(const bool pretty)
{
	pt.put("status", Response::ToString(status));
	std::stringstream ss;
	boost::property_tree::write_json(ss, pt, pretty);
	
	return ss.str();
}
