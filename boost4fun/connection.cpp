#include "connection.h"
#include "parser.h"


namespace tcp
{
	connection::connection(boost::asio::io_service& io_service)
		: socket_(io_service)
		, handler_{*this}
		, router_{ handler_ }
	{}

	connection::pointer connection::create(boost::asio::io_service & io_service)
	{
		return pointer(new connection(io_service));
	}

	boost::asio::ip::tcp::socket & connection::socket()
	{
		return socket_;
	}

	void connection::handle_input_message()
	{
		std::istream is(&input_buffer);
		auto root = parsing::parse_json(is);
		router_.handle(root);
	}

	void connection::handle()
	{
		boost::asio::async_read_until(socket_, input_buffer, '\n',
			[this, _ = shared_from_this()](auto& error, auto size)
		{
			handle_input_message();
		});
		//boost::asio::async_read(socket_, input_buffer, boost::asio::transfer_all(),
		//	[this, _ = shared_from_this()](auto& error, auto size)
		//{
		//	handle_input_message();
		//});

	}
	std::string & connection::output_msg()
	{
		return output_message;
	}


}//namespace tcp
