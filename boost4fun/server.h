#pragma once

#include <boost/asio.hpp>

#include "connection.h"

namespace tcp
{
	class server
	{
	public:
		server(boost::asio::io_service& io_service, unsigned port);

	private:
		void accept();
		boost::asio::ip::tcp::acceptor acceptor_;
	};
}//namespace tcp
