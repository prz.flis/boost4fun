#pragma once

#include "parser.h"


namespace parsing
{
	parser::parser(boost::property_tree::ptree& ptree)
		: root{ ptree }
	{}

	boost::property_tree::ptree parse_json(std::istream & input_stream)
	{
		boost::property_tree::ptree root;
		boost::property_tree::read_json(input_stream, root);
		return root;
	}

	unsigned get_msgId(boost::property_tree::ptree & pt)
	{
		return pt.get<unsigned>("msgId");
	}

}