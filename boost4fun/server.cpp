#include "server.h"

namespace tcp
{
	server::server(boost::asio::io_service& io_service, unsigned port)
		: acceptor_(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
	{
		accept();
	}

	void server::accept()
	{
		connection::pointer new_connection = connection::create(acceptor_.get_io_service());

		acceptor_.async_accept(new_connection->socket(),
			[=](const auto& error)
		{
			if (!error)
			{
				new_connection->handle();
			}
			else
			{
				std::cerr << error << std::endl;
			}
			accept();
		});
	}
} //namespace tcp
