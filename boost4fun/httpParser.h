#pragma once

#include <string>
#include <map>
#include <vector>
#include <algorithm>

#include "CppUnitTestLogger.h"
namespace http
{
	class parser
	{
	public:
		static auto parse_request(const std::string& req)
		{
			std::map<std::string, std::vector<std::string>> header;
			std::size_t pos{ 0 };
			while(1)
			{
				using namespace Microsoft::VisualStudio::CppUnitTestFramework;
				auto old_pos = pos;
				pos = req.find("\t\t\t", old_pos);
				std::string sub = req.substr(old_pos, pos - old_pos);
				sub.erase(std::remove_if(sub.begin(), sub.end(), isspace), sub.end());
				pos += 3;
				Logger::WriteMessage(sub.c_str());
			}
			return 3;
		}
	};
}