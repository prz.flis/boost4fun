import socket
import threading
import time
import json

PORT = 9987
HOST = "localhost"

def make_socket():
    return socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def connect(soc):
    soc.connect((HOST, PORT))

def send(soc, msg):
    soc.send(msg + b'\n')

def make_example_msg():
    return {
                "msgId" : 0x01,
                "payload":{
                "first_name":"przemek",
                "last_name":"flis",
                "age":22,
                "girlfriend": "misia"
                }
            }

def test(json_maker, max = 1, do_printing = True):
    for i in range(max):
        soc = make_socket()
        connect(soc)
        json_data = json_maker()
        str_data = json.dumps(json_data)

        input = str_data.encode("utf-8")
        if do_printing:
            print(input)
        send(soc, input)
        data = soc.recv(100)
        if do_printing:
            print(data)
        # assert(data == b'handler success')

def stress_test(json_maker, numOfThreads = 10):
    timer = time.perf_counter
    ths = []
    for i in range(numOfThreads):
        ths.append(threading.Thread(target=test, args=(json_maker, 500,False)))
    start = timer()
    for th in ths:
        th.start()
    for th in ths:
        th.join()
    end = timer()

    print(end-start)
if __name__ == '__main__':

    test(make_example_msg)
    for i in range(10):
        stress_test(make_example_msg)


