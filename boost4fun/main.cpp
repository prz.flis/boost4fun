#include <boost/asio.hpp>

#include "server.h"

int main()
{
	try
	{
		boost::asio::io_service io_service;
		tcp::server server(io_service, 9987);
		io_service.run();
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	system("pause");
	return 0;
}