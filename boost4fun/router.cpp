#include "router.h"
#include "parser.h"

namespace Request
{
	Router::Router(Handler & handler_)
		: handler_(handler_)
	{
	}
	void Router::handle(boost::property_tree::ptree & root)
	{
		const auto msgId = parsing::get_msgId(root);

		switch (msgId)
		{
		case 0x01:
			handler_.handle(parsing::parser(root).get<msg::example_msg>());
			break;
		default:
			//TODO :) 
			break;
		}
	}
}

