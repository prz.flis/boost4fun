#pragma once

#include <string>

namespace Response
{
	enum class Status
	{
		OK,
		NOT_OK
	};

	std::string ToString(const Status status);
}

