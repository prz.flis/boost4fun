#pragma once

#include "Iconnection.h"

namespace Request
{
	class IResponseSender
	{
	public:
		IResponseSender(tcp::Iconnection& connection)
			: connection{ connection }
		{}
		virtual void sendResponse(std::string&& resp) = 0;
	protected:
		tcp::Iconnection& connection;
	};
}
