#pragma once

#include <string>
#include <sstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "example_msg.h"

namespace parsing
{
	class parser
	{
	public:
		parser(boost::property_tree::ptree& ptree);

		template<typename MsgType>
		MsgType get() { {} };

		template<>
		msg::example_msg get<msg::example_msg>()
		{
			return {
				root.get<std::string>("payload.first_name"),
				root.get<std::string>("payload.last_name"),
				root.get<unsigned>("payload.age"),
				root.get<std::string>("payload.girlfriend"),
			};
		}
	private:
		boost::property_tree::ptree& root;
	};

	boost::property_tree::ptree parse_json(std::istream& input_stream);

	unsigned get_msgId(boost::property_tree::ptree& pt);
}
