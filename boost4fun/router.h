#pragma once

#include <boost\property_tree\ptree.hpp>

#include "handler.h"

namespace Request
{
	class Router
	{
	public:
		Router(Handler & handler_);

		void handle(boost::property_tree::ptree & root);
	private:
		Handler& handler_;
	};
}