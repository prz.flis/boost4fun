#pragma once

#include "example_msg.h"

#include <iostream>

#include "Iconnection.h"
#include "IResponseSender.h"

namespace Request
{
	class Handler : public IResponseSender
	{
	public:
		Handler(tcp::Iconnection& connection);
		void handle(const msg::example_msg& msg);
		void sendResponse(std::string&& resp) override;
	};
}
