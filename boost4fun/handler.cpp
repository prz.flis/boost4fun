#include "handler.h"
#include "ResponseBuilder.h"
#include "database.h"

Request::Handler::Handler(tcp::Iconnection & connection)
	: Request::IResponseSender{ connection }
{
}

void Request::Handler::handle(const msg::example_msg & msg)
{
	database::get().increment();
	sendResponse(Response::ResponseBuilder{ Response::Status::OK }.getJsonString());
}

void Request::Handler::sendResponse(std::string&& resp)
{
	boost::asio::async_write(connection.socket(), boost::asio::buffer(resp),
		[_ = connection.shared_from_this()](auto& error, auto nomOfBytes)
	{
		if (error)
		{
			std::cerr << error << std::endl;
		}
	});
}
