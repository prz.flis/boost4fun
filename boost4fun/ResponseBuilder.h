#pragma once

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "ResponseStatus.h"
#include <string>

namespace Response
{
	class ResponseBuilder
	{
	public:
		ResponseBuilder(const Response::Status status);
		std::string getJsonString(const bool pretty = false);
	private:
		boost::property_tree::ptree pt;
		Response::Status status;
	};
}