#pragma once

#include <string>

namespace msg
{
	struct example_msg
	{
		std::string first_name;
		std::string last_name;
		unsigned age;
		std::string girlfriend;
	};
	inline bool operator==(const example_msg& left, const example_msg& right)
	{
		return left.first_name == right.first_name
			&& left.last_name == right.last_name
			&& left.age == right.age
			&& left.girlfriend == right.girlfriend;
	}
}