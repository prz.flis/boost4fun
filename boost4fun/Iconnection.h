#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio.hpp>

#include <string>

namespace tcp
{
	class Iconnection
		: public boost::enable_shared_from_this<Iconnection>
	{
	public:
		virtual boost::asio::ip::tcp::socket& socket() = 0;
		virtual void handle_input_message() = 0;
		virtual std::string& output_msg() = 0;
		virtual void handle() = 0;

	};
} //namespace tcp