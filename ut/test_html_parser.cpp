#include <string>
#include <sstream>
#include <iostream>

#include "stdafx.h"
#include "CppUnitTest.h"

#include "../boost4fun/httpParser.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ut
{
	TEST_CLASS(TestHttpParser)
	{
	public:

		std::string req = "GET /hello.htm HTTP/1.1 \
			User - Agent : Mozilla / 4.0 (compatible; MSIE5.01; Windows NT)\
			Host : www.tutorialspoint.com\
			Accept - Language : en - us\
			Accept - Encoding : gzip, deflate\
			Connection : Keep - Alive";

		TEST_METHOD(parsingHttpRequest)
		{
			http::parser::parse_request(req);
		}
	};
}