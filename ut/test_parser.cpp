#include <string>
#include <sstream>
#include <iostream>

#include "stdafx.h"
#include "CppUnitTest.h"

#include "../boost4fun/parser.cpp"
#include "../boost4fun/example_msg.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ut
{		
	TEST_CLASS(Parser)
	{
	public:
		
		auto make_example_msg_json_stringstream(msg::example_msg obj, unsigned msgId = 0x01)
		{
			std::stringstream ss;
			std::string json;

			ss << "{ "
				<< "\"msgId\" :" << msgId << ","
				<< "\"payload\" :"
				<< "{ "
				<< "\"first_name\" :" << "\"" << obj.first_name << "\","
				<< "\"last_name\" :" << "\"" << obj.last_name << "\","
				<< "\"age\" :" << obj.age << ","
				<< "\"girlfriend\" :" << "\"" << obj.girlfriend<<"\""
				<< "}"
				<< "}" << std::endl;

			//std::getline(ss, json);
			//Logger::WriteMessage(json.c_str());
			return ss;
		}

		TEST_METHOD(parsingExampleMsgJsonPayload)
		{

			msg::example_msg msg{ "john", "cena", 22, "pocahontaz" };
			auto msg_ss = make_example_msg_json_stringstream(msg);
			boost::property_tree::ptree root;
			boost::property_tree::read_json(msg_ss, root);

			auto result = parsing::parser(root).get<msg::example_msg>();
	
			Assert::IsTrue(msg == result);

		}
		TEST_METHOD(parsingExampleMsgJsonMsgId)
		{
			unsigned expected = 0x01;

			msg::example_msg msg{ "john", "cena", 22, "pocahontaz" };
			auto msg_ss = make_example_msg_json_stringstream(msg, expected);
			boost::property_tree::ptree root;
			boost::property_tree::read_json(msg_ss, root);

			auto result = parsing::get_msgId(root);

			Assert::AreEqual(expected, result);
		}
	};
}