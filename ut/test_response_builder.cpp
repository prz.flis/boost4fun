#include <string>
#include <sstream>
#include <iostream>

#include "stdafx.h"
#include "CppUnitTest.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <sstream>

#include "../boost4fun/ResponseBuilder.cpp"
#include "../boost4fun/ResponseStatus.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ut
{
	TEST_CLASS(ResponseBuilder)
	{
	public:

		void test_status(const Response::Status status, std::string correct_value)
		{
			Response::ResponseBuilder rb{ status };
			std::stringstream ss;
			ss << rb.getJsonString();
			boost::property_tree::ptree pt;
			boost::property_tree::read_json(ss, pt);
			Assert::AreEqual(pt.get<std::string>("status"), correct_value);
		}

		TEST_METHOD(responseStatusString)
		{
			test_status(Response::Status::OK, "OK");
			test_status(Response::Status::NOT_OK, "NOT_OK");
		}

	};
}